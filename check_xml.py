from bs4 import BeautifulSoup
from bs4.element import Tag
from rich import print
with open('Jurisdiction_xml\\SD_City_Neighborhoods_2012.xml') as f:
    soup = BeautifulSoup(f.read(), 'html.parser')
    pubdate = soup.find('pubdate')
    formatname = soup.find('formatname')

    print(pubdate.text) # date - under what circumstances would this differ from dateIssued
    print(formatname.text) # --> Shapefile
