'''
Requirements
    * No heavy uses (an absolute maximum of 1 request per second).
    * Provide a valid HTTP Referer or User-Agent identifying the application (stock User-Agents as set by http libraries will not do).
    * Clearly display attribution as suitable for your medium.
    * Data is provided under the ODbL license which requires to share alike (although small extractions are likely to be covered by fair usage / fair dealing).

https://operations.osmfoundation.org/policies/nominatim/
'''

from geopy.geocoders import Nominatim
from rich import print

geolocator = Nominatim(user_agent="testing_geopy")
location = geolocator.reverse("32.959875, -117.256026")

print(location.raw)
print(type(location.raw))
county = location.raw['address']['county']
print(county)
# split_address = location.address.split(',')
# print(split_address)
# print(location.address)
# print(type(location.address))
